package main

import "gorm.io/gorm"

type Work struct {
	gorm.Model
	URL             string `json:"URL,omitempty"`
	WorkID          string `json:"WorkID,omitempty"`
	ChapterID       string `json:"ChapterID,omitempty"`
	ChapterTitle    string `json:"ChapterTitle,omitempty"`
	Title           string `json:"Title,omitempty"`
	Author          string `json:"Author,omitempty"`
	Published       string `json:"Published,omitempty"`
	Updated         string `json:"Updated,omitempty"`
	Words           string `json:"Words,omitempty"`
	Chapters        string `json:"Chapters,omitempty"`
	Comments        string `json:"Comments,omitempty"`
	Kudos           string `json:"Kudos,omitempty"`
	Bookmarks       string `json:"Bookmarks,omitempty"`
	Hits            string `json:"Hits,omitempty"`
	Fandom          string `json:"Fandom,omitempty"`
	Summary         string `json:"Summary,omitempty"`
	ChaptersTitles  string `json:"ChaptersTitles,omitempty" gorm:"type:text"`
	ChaptersIDs     string `json:"ChaptersIDs,omitempty" gorm:"type:text"`
	Relationship    string `json:"Relationship,omitempty" gorm:"type:text"`
	AlternativeTags string `json:"AlternativeTags,omitempty" gorm:"type:text"`
	Downloads       string `json:"Downloads,omitempty" gorm:"type:text"`
}
