package internals

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"regexp"
	"strings"

	ao3 "gitlab.com/capoverflow/ao3api"
	"gitlab.com/capoverflow/ao3api/models"
)

type ID struct {
	WorkID    string
	ChapterID string
}

func RemoveDuplicateID(sample []ID) []ID {
	var unique []ID

	for _, v := range sample {
		skip := false
		for _, u := range unique {
			if v.WorkID == u.WorkID {
				skip = true
				break
			}
		}
		if !skip {
			unique = append(unique, v)
		}
	}
	return unique

}

func RemoveDuplicateAuthor(sample []string) []string {
	var unique []string

	for _, v := range sample {
		skip := false
		for _, u := range unique {
			if v == u {
				skip = true
				break
			}
		}
		if !skip {
			unique = append(unique, v)
		}
	}
	return unique

}

//SliceFind search string in slice of string
func SliceFind(slice []string, val string) (int, bool) {
	for i, item := range slice {

		if item == val {
			return i, true
		}
	}
	return -1, false
}

//SliceIndex Get position of element in slice
func SliceIndex(limit int, predicate func(i int) bool) int {
	for i := 0; i < limit; i++ {
		if predicate(i) {
			return i
		}
	}
	return -1
}

// DeleteEmpty ...
func DeleteEmpty(s []string) []string {
	var r []string
	for _, str := range s {
		if str != "" {
			r = append(r, str)
		}
	}
	return r
}

// ReadFile read from file
func GetFanficURL(fanfic string) (fanfics string) {
	//var chaps []string
	// var wID string
	// var wChap string
	//	var a int

	//archive := regexp.MustCompile(`https?:\/\/archiveofourown.org\/works\/[0-9]+`)
	archive := regexp.MustCompile(`https?://archiveofourown.org/works/[0-9]+(?:/chapters/[0-9]+)?`)
	fanfics = archive.FindString(fanfic)
	// return idsWork
	return fanfics
}

func GetFanficID(fanfics []string) (idsWork []ID) {

	var wID, wChap string
	var idWork ID
	// var idsWork []ID

	fanfics = DeleteEmpty(fanfics)
	//log.Println(len(fanfics))
	for i := range fanfics {
		//log.Println(i)
		//a = a+i
		splitPath := strings.Split(fanfics[i], "/")
		works := SliceIndex(len(splitPath), func(i int) bool { return splitPath[i] == "works" })
		chapters := SliceIndex(len(splitPath), func(i int) bool { return splitPath[i] == "chapters" })
		//log.Println(works, chapters)
		if len(splitPath) == 5 {
			//log.Println("Oneshot")
			//log.Println(splitPath[works+1])
			wID = splitPath[works+1]
			//log.Println(len(wID), wID)
			idWork.WorkID = wID
			idWork.ChapterID = ""
			idsWork = append(idsWork, idWork)
		} else if len(splitPath) == 7 {
			//print("MultiChapters")
			// log.Println(splitPath[works+1], splitPath[chapters+1])
			wID = splitPath[works+1]
			wChap = splitPath[chapters+1]
			idWork.WorkID = wID
			idWork.ChapterID = wChap
			//log.Println(wID)
			idsWork = append(idsWork, idWork)
		}
		//log.Println(idsWork)
	}
	return idsWork

}

func DoingParsing(wID, cID string) models.Work {
	Parser := ao3.Fanfic
	// var status int
	// time.Sleep(2 * time.Second)

	FinishedFanfic, status, err := Parser(wID, cID, false, []string{})

	if err != nil || status == 404 {
		log.Println(err, status, wID)
	} else {
		return FinishedFanfic
	}
	return FinishedFanfic

}

// Download file from stackoverflow
func DownloadFile(filepath string, url string) (err error) {

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Check server response
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("bad status: %s", resp.Status)
	}

	// Writer the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}

	return nil
}
