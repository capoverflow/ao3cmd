package internals

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/capoverflow/ao3api/models"
)

func Fanfic(cmd *cobra.Command, args []string) {
	path, _ := cmd.Flags().GetString("path")
	dstatus, _ := cmd.Flags().GetBool("download")
	var fanfic models.Work
	if len(args) > 1 {
		ff := DoingParsing(args[0], args[1])
		fanfic = ff
	} else {
		ff := DoingParsing(args[0], "")
		fanfic = ff
	}
	if dstatus {
		// log.Println(dstatus)
		// log.Println(fanfic.Title)
		if len(path) != 0 {
			err := DownloadFile(path, fanfic.Downloads[3])
			if err != nil {
				log.Println(err)
			}
		} else {
			if _, err := os.Stat("./downloads"); os.IsNotExist(err) {
				err := os.Mkdir("./downloads", 0755)
				if err != nil {
					log.Fatal(err)
				}

			}
			err := DownloadFile(fmt.Sprintf("./downloads/%s.pdf", fanfic.Title), fanfic.Downloads[3])
			if err != nil {
				log.Println(err)
			}
		}

	} else {
		body, _ := json.MarshalIndent(fanfic, "", "\t")
		fmt.Println(string(body))
	}

}
