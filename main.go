/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package main

import (
	"ao3cmd/cmd"
	"log"
	"os"
)

func init() {
	// log.SetFlags(0)
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.SetPrefix("ao3cmd: ")
	log.SetOutput(os.Stderr)
}

func main() {
	cmd.Execute()
}
